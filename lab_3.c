#include <stdio.h>
#include <math.h>


int main()
{
    double x = 0;
    double a = 0;
    printf("Введите величину шага:");
    scanf("%lf", &a);
    printf("   x          f(x)\n");
    printf("--------------------\n");

    do
    {
        double y = 8*pow(x, 3) *cos(x);
        printf("%lf%s%lf\n",x,"    ",y);
        x=x+a;
    } while (floor(x)!=1.0);
    for (double i = 1.1; i <= 2.1; i=i+a)
    {
        double z = log(1+sqrt(i))-cos(i);
        printf("%lf%s%lf\n",i,"    ",z);
    }

}