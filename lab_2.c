#include <stdio.h>
#include <math.h>


int main()
{
    double x;
    printf("Enter x { 0.1 <= x <= 1.3 } -> ");
    scanf("%lf", &x);
    if (0.1 <= x && x <= 1.3)
    {
        double y = 5 * sqrt (sin(sqrt(x)));
        double z = log10(cos(log(y)));
        printf("y(x) = %lf\nz(y) = %lf\n", y, z);
    }
    else
        printf("x value is incorrect!\n");
    return 0;
}
